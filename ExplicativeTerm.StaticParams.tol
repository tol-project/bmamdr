//ExplicativeTerm.StaticParams.tol
  
//////////////////////////////////////////////////////////////////////////////
Class @ExplicativeTerm.StaticParams : @ExplicativeTerm
//////////////////////////////////////////////////////////////////////////////
{
  //////////////////////////////////////////////////////////////////////////////
  Static NameBlock Create(
    Serie input_, 
    @Features features_, 
    Serie trnsDomain_, 
    @TransferFunction trnsFunct_)
  //////////////////////////////////////////////////////////////////////////////
  {
    @ExplicativeTerm.StaticParams aux = [[
      Text _.name = If(features_::isAdditive,"Add.","Trf.")+Name(input_);
      @Serie _.input = @Serie(input_);
      @TransferFunction _.trnsFunct = trnsFunct_;
      @Serie _.trnsDomain = @Serie(trnsDomain_);
      @Features _.features = features_;
      Date _.start = TheEnd;
      Date _.stop  = TheBegin;
      Set _.index = Copy(Empty);
      Set _.parent.index = Copy(Empty);
      Real _.n = ?;
      Real _.m = ?;
      Real _.r = ?;
      Real _.p = ?;
      VMatrix _.b = Constant(0,0,0); 
      VMatrix _.X = Constant(0,0,?);
      VMatrix _.A = Constant(0,0,?);
      VMatrix _.a = Constant(0,0,?);
      VMatrix _.G = Constant(0,0,?);
      VMatrix _.g = Constant(0,0,?)
    ]];
    Eval(aux::_.name+"=aux")
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Build(Date start_, Date stop_)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text prf = "";
  //Text prf = ;
    Date _.start := start_;
    Date _.stop  := stop_;
    Real BuildTargets(?);
    WriteLn("[bMamDr] Building "+If(_.k==0,"",""<<_.k+" instances of ")+
     "ExplicativeTerm.StaticParams "+_.name+" "+
     If(_.features::isAdditive,"(ADDITIVE)","(NON ADDITIVE)")+
     " since "<<_.start+" to "<<_.stop);
    Real _.m := DateDif(Dating($_.input),_.start,_.stop)+1;
    Real _.n := _.trnsFunct::_.n;
    Real _.r := _.trnsFunct::_.r;
    Real _.p := _.trnsFunct::_.p;
    Set _.index := EvalSet(_.trnsFunct::_.index,Real(Real k)
    {
      Eval(_.name+"_"+Name(k)+"=k")
    });
    VMatrix _.b := _.trnsFunct::_.b0; 
    VMatrix _.X := If(_.features::hasDynDomain,
    {
      BinGroup("+", For(1,_.k,VMatrix(Real k)
      {
        Set it = _.inpTrg[k];
        Set inp = _.trnsFunct::Instance.SeriesForDate(
          $_.trnsDomain, _.start, _.stop, it::target, it::value);
        If(k%50==0,WriteLn(" "<<k));
        VMatrix x = Mat2VMat(SerSetMat(inp),True);
        x
      }))
    },
    {
      Set inp = _.trnsFunct::ApplyShapes($_.input,_.start,_.stop);
      Mat2VMat(SerSetMat(inp),True)
    });
    WriteLn("");
    Real SetIndexByName(_.index); 
    VMatrix _.A := _.trnsFunct::Get.A(?);
    VMatrix _.a := _.trnsFunct::Get.a(?);
    VMatrix _.G := _.trnsFunct::Get.G(?);
    VMatrix _.g := _.trnsFunct::Get.g(?);
    
    If(VMatSum(IsUnknown(_.A)), Error("[bMamDr] "+_.name+" constraint matrix 'A' has "<<
      VMatSum(IsUnknown(_.A))+" unknown values"));
    If(VMatSum(IsUnknown(_.a)), Error("[bMamDr] "+_.name+" border matrix 'a' has "<<
      VMatSum(IsUnknown(_.a))+" unknown values"));
    If(VMatSum(IsUnknown(_.G)), Error("[bMamDr] "+_.name+" prior matrix 'G' has "<<
      VMatSum(IsUnknown(_.G))+" unknown values"));
    If(VMatSum(IsUnknown(_.g)), Error("[bMamDr] "+_.name+" prior mean 'g' has "<<
      VMatSum(IsUnknown(_.g))+" unknown values"));

    VMatrix chk.G = CholeskiFactor(_.G,"XtX",True,False);
    If(!VRows(chk.G),
    {
      Warning("[bMamDr] "+_.name+" Prior matrix 'G' has no full rank ")
    });
      
    If(VRows(_.A),
    {
      VMatrix Aba = _.A*_.b-_.a;
      Real maxAba = VMatMax(Aba);
      If(maxAba>0,
      {
        Error("[bMamDr] Initial solution is not feasible for "+
              "explicative term "<<_.name+". Border Distance:"<<maxAba)
      })
    });      
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Set PriorAndConstraintsStatus(VMatrix beta, Real maxDeep)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [PriorAndConstraintsStatus] "+_.name);
    Set txl = TaxonList(maxDeep)<<[[
      Text Concept  = _.name,
      Text Transfer = _.trnsFunct::_.name, 
      Text TargetDate = ""]];
    VMatrix b = SubRow(beta,_.parent.index);
    _.trnsFunct::PriorAndConstraintsStatus(txl,b)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Set BetaStats(Matrix mcmc, Real maxDeep)
  ////////////////////////////////////////////////////////////////////////////
  {
    WriteLn("Collecting stats of Static Explicative Term "+_.name);
    Text ParamGroup=If(_.features::isAdditive,"AddInput","TrfInput");
    Set txl = [[ ParamGroup]] << TaxonList(maxDeep)<<[[
      Text Concept  = _.name,
      Text Transfer = _.trnsFunct::_.name, 
      Text TargetDate = ""]];
    Matrix mcmc.1 = SubCol(mcmc,_.parent.index);
    _.trnsFunct::BetaStats(txl,mcmc.1)
  }   
};

