//////////////////////////////////////////////////////////////////////////////
Class @Forecast : @ModelDefine
//////////////////////////////////////////////////////////////////////////////
{
  @NameBlock _.estim;
  Set _.quantilePoints = SetOfReal(.05,.10,.20,.80,.90,.95);
  Set _.results = Copy(Empty);
  
  ////////////////////////////////////////////////////////////////////////////
  Static @Forecast Create(
    NameBlock define,  
    NameBlock estim, 
    Date start, 
    Date stop,
    Real forcePredictible)
  ////////////////////////////////////////////////////////////////////////////
  {      
    @Forecast forecast = 
    [[
      @NameBlock _.estim = @NameBlock(estim)
    ]];
    Real forecast::CopyDefinition(define);
    Text forecast::_.userEquations := "";
    Real forecast::BuildTransferFunctions(start, stop, True, forcePredictible);
    Set  forecast::_.audit := forecast::Audit(?);
    forecast
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Set ExecuteForecast(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text prf = "";
  //Text prf = $_.estim::_.name+"_";
    Set markovChains.est = $_.estim::_.diagnostic::markovChains;
    Set markovChains.for_ = SetConcat(For(1,Card(_.expTrmAll),Set(Real j){
      NameBlock et.for = _.expTrmAll[j];
      WriteLn("Building Forecast Markov Chain for explicative term \n  "+
        et.for::_.name+" ...");
      NameBlock et.est = $_.estim::_.expTrmAll[et.for::_.name];
      NameBlock _.trnsFunct = et.for::_.trnsFunct;
      Set SetConcat(For(1,Card(_.trnsFunct::_.index),Set(Real k) {
        Text shape = Name(_.trnsFunct::_.index[k]);
        If(et.est::_.features::hasDynParams,
        {
          Set mcmc.est = SetConcat(EvalSet(et.est::_.targets,Set(Date trg)
          {
            Text id = prf+et.est::_.name+"_"<<trg+"_"+shape;
            Real found = FindIndexByName(markovChains.est,id);
            If(!found,
            {
              Warning("Markov Chain not found for \n  "+id);
              Empty
            }, 
            {
              [[ markovChains.est[found] ]]
            })
          }));
          Matrix mcmc.mean = SetSum(mcmc.est)*1/Card(mcmc.est);
          Set mcmc.for = EvalSet(et.for::_.targets,Matrix(Date trg)
          {
            Text idF = prf+et.for::_.name+"_"<<trg+"_"+shape;
            Eval(idF+"=mcmc.mean")
          })
        },
        {
          Text id = prf+et.est::_.name+"_"+shape;
          Real found = FindIndexByName(markovChains.est,id);
          If(!found,
          {
            Warning("Markov Chain not found for \n  "+id);
            Empty
          }, 
          {
            Text idF = prf+et.for::_.name+"_"+shape;
            [[Eval("Matrix "+idF+"=markovChains.est[found]")]]
          })
        })
      }))
    }));
    Real SetIndexByName(markovChains.for_);
    Set markovChains.for = EvalSet(_.index,Matrix(Real idx)
    {
      markovChains.for_[Name(idx)]
    });
    Real S = Rows(markovChains.est[1]);  
    VMatrix mcmc.for = If(!Card(markovChains.for),Constant(0,S,0),
      Mat2VMat(Group("ConcatColumns",markovChains.for),True));
    Real n.add = Card(_.indexAdd);
    Real n.trf = Card(_.indexTrf);
    VMatrix _.b := SetCol(EvalSet(markovChains.for,MatAvr));
    Set trf = If(!n.trf,
    [[
      VMatrix flt.trf = Constant(_.m,1,0);
      VMatrix mcmc.flt.trf = Constant(_.m,S,0)
    ]],
    [[
      VMatrix b.trf = SubRow(_.b, _.indexTrf);
      VMatrix mcmc.trf = Group("ConcatColumns",NCopy(S,b.trf));
    //VMatrix mcmc.trf = SubRow(mcmc.for, _.indexTrf);
      VMatrix X.trf = SubCol(_.X, _.indexTrf);
      VMatrix flt.trf = X.trf*b.trf+$_.estim::_.Z1.trf;
      VMatrix mcmc.flt.trf = X.trf*mcmc.trf+$_.estim::_.Z1.trf
    ]]);
    Set add = If(!n.add,
    [[
      VMatrix flt.add = Constant(_.m,1,0);
      VMatrix mcmc.flt.add = Constant(_.m,S,0)
    ]],
    [[
      VMatrix b.add = SubRow(_.b, _.indexAdd);
      VMatrix mcmc.add = Group("ConcatColumns",NCopy(S,b.add));
    //VMatrix mcmc.add = SubRow(mcmc.for, _.indexAdd);
      VMatrix X.add = SubCol(_.X, _.indexAdd);
      VMatrix flt.add = X.add*b.add+$_.estim::_.Z1.add;
      VMatrix mcmc.flt.add = X.add*mcmc.add+$_.estim::_.Z1.add
    ]]);
    
    @NameBlock sampler = If(Card($_.estim::_.samplerTrf),
      $_.estim::_.samplerTrf,
      $_.estim::_.samplerAdd);
    Real Z1 = If(Card($_.estim::_.samplerTrf),
      $_.estim::_.Z1.trf,
      $_.estim::_.Z1.add);
 
    Polyn dif =$sampler::dif;
    Polyn ar = $sampler::ar;
    Polyn ma = $sampler::ma;
    VMatrix sigma.for =  Mat2VMat((DifEq(1/(1-B),
      PolMat(Expand(ma/(ar*dif),_.m),_.m,1)^2))^.5 * 
       $sampler::sigma);
  //WriteLn("TRACE [ExeFcst] _.start:"<<_.start);   
    Date org = Succ(_.start,_.timeInfo->Dating,-1);
  //WriteLn("TRACE [ExeFcst] org:"<<org);   
    Real delay = DateDif(_.timeInfo->Dating,org,$_.estim::_.stop);
  //WriteLn("TRACE [ExeFcst] delay:"<<delay);   
    Real origin = $sampler::m-delay;
  //WriteLn("TRACE [ExeFcst] origin:"<<origin);   
    Real p = $sampler::d+$sampler::p;
  //WriteLn("TRACE [ExeFcst] p:"<<p);   
    Real q = $sampler::q;
  //WriteLn("TRACE [ExeFcst] q:"<<q);   
    Set rng.p = Range(origin-p+1,origin,1);
    Set rng.q = Range(origin-q+1,origin,1);
    VMatrix z.past = SubRow($sampler::Z-Z1,rng.p);
    VMatrix e.past = SubRow($sampler::E,rng.q);

    VMatrix mcmc.noise = Group("ConcatColumns",For(1,S,VMatrix(Real iter){
      VMatrix e.fut = Gaussian(_.m,1,0, $sampler::sigma);
      VMatrix z.fut = DifEq(ma/(dif*ar),e.fut,e.past,z.past)
    }));
      
    Matrix mcmc.forecast = Max(VMat2Mat(
      Transform.Inverse.V(mcmc.noise+mcmc.flt.trf)+mcmc.flt.add),
      If($_.estim::_.nonNegOut,0,-1/0));

    Matrix quantile.def = SetCol(_.quantilePoints);

    Set fcst = If(!_.m,Copy(Empty),
    MatSerSet(Group("ConcatColumns",For(1,_.m,Matrix(Real j)
    {
      Matrix mcmc = Tra(SubRow(mcmc.forecast,[[j]]));
      Real mean = MatAvr(mcmc);
      Col(mean)<<Quantile(mcmc,quantile.def)
    })),_.timeInfo->Dating,_.timeInfo->FirstDate));
    
    Set forecast.series = 
    [[
      Serie $_.estim::_.output[1],
      Serie Forecast.Mean = fcst[1]
    ]] << For(1,Card(_.quantilePoints),Serie(Real j)
    {
      Text qt = FormatReal(_.quantilePoints[j],"%5.3lf");
      Eval("Forecast.Q"<<qt+"=fcst[j+1]")
    })<<
    [[
      Serie hist.forecast.add = 
        $_.estim::_.diagnostic::all.series::histForecast.add;
      Serie filter.trf = vMat2Ser(flt.trf),
      Serie hist.filter.trf = 
        $_.estim::_.diagnostic::all.series::filter.trf;
      Serie noise.trf  = vMat2Ser(mcmc.noise*Constant(S,1,1/S)),
      Serie hist.noise.trf = 
        $_.estim::_.diagnostic::all.series::noise.trf;
      Serie forecast.trf = filter.trf+noise.trf;
      Serie hist.forecast.trf = 
        $_.estim::_.diagnostic::all.series::histForecast.trf;
      Serie forecast.trf.add = Transform.Inverse.S(forecast.trf);
      Serie hist.forecast.trf.add = 
        $_.estim::_.diagnostic::all.series::histForecast.trf.add;
      Serie filter.trf.add = forecast.trf.add-Transform.Inverse.S(noise.trf);
      Serie hist.filter.trf.add = 
        $_.estim::_.diagnostic::all.series::filter.trf.add;
      Serie filter.add = vMat2Ser(flt.add);
      Serie hist.filter.add = 
        $_.estim::_.diagnostic::all.series::filter.add;
      Serie sigma.evolution =  vMat2Ser(sigma.for)
    ]];

    Set _.results := [[
      Set eqStatus = PriorAndConstraintsStatus(_.b);
      Set markovChains = markovChains.for;
      Set paramStats = ParamStats(VMat2Mat(mcmc.for,True), 
        $sampler, $($sampler::auto.burn));
      Set dynamicParameters = _.dynamicParameters;
      Set effects.flat = If(!_.buildEffects,Copy(Empty),GetAllEffects(_.b,True));
      Set effects.tree = If(!_.buildEffects,Copy(Empty),GetAllEffects(_.b,False));
      Set forecast.series;
      Set all.series = 
      {
        Set flat = PlainNamedObjects(forecast.series<<effects.tree);
        Select(flat,Real(Anything obj)
        {
          Grammar(obj)=="Serie"
        })
      }
    ]]    
  }
    
};  
  
