//SimpleShapes.tol

//////////////////////////////////////////////////////////////////////////////
Class @Shape
//////////////////////////////////////////////////////////////////////////////
{
  Text _.name;
  Real _.minLag;
  Real _.maxLag;
  Polyn _.omega;
  VMatrix _.X
};

//////////////////////////////////////////////////////////////////////////////
NameBlock OmegaShape(
  Text name_,
  Polyn omega_)
//////////////////////////////////////////////////////////////////////////////
{
  @Shape aux = [[
    Text _.name = name_,
    Polyn _.omega = omega_,
    Set _.monomes = Monomes(_.omega);
    Real _.numMon = Card(_.monomes);
    Real _.minLag = Degree(_.monomes[1]),
    Real _.maxLag = Degree(_.monomes[_.numMon]),
    VMatrix _.X = Mat2VMat(SetCol(For(_.minLag,_.maxLag,Real(Real d) 
    { Coef(_.omega,d) })))
  ]];
  Eval(aux::_.name+"=aux")
};

//////////////////////////////////////////////////////////////////////////////
NameBlock DeltaShape(
  Text name_,
  Real delta_,
  Real minLag_,
  Real maxLag_,
  Real dropValue_)
//////////////////////////////////////////////////////////////////////////////
{
  @Shape aux = [[ 
    Text _.name = name_,
    Real _.dropValue = dropValue_, 
    Real _.minLag = minLag_,
    Real _.maxLag = maxLag_,
    Real _.maxDeg = Max(Abs(_.maxLag),Abs(_.minLag));
    Real _.delta = If(!IsUnknown(delta_),delta_,_.dropValue^(1/(1+_.maxDeg))),
    Ration _.ratio = (1-_.delta)/(1-_.delta*B);
    Polyn _.omega = {
      Real len = _.maxLag-_.minLag+1;
      Polyn expand = Expand(_.ratio,len);
      Matrix coef = PolMat(expand,len,1);
      Matrix coef.drop_ = IfMat(LT(Abs(coef),_.dropValue/10),0,coef);
      Matrix coef.drop = coef.drop_*1/MatSum(coef.drop_);
      Polyn pol = MatPol(Tra(coef.drop));
      Case(
        And(_.minLag< 0, _.maxLag==0), ChangeBF(pol),
        And(_.minLag< 0, _.maxLag< 0), ChangeBF(pol)*(F^-_.maxLag),
        And(_.minLag==0, _.maxLag> 0), pol,
        And(_.minLag> 0, _.maxLag> 0), pol*(B^_.minLag) )
    };
    VMatrix _.X = Mat2VMat(SetCol(For(_.minLag,_.maxLag,Real(Real d) 
    { Coef(_.omega,d) })))
  ]];
  Eval(aux::_.name+"=aux")
};



